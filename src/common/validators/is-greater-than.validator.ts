import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
} from 'class-validator';

export function isGreaterThanValidate(
  value: unknown,
  args: ValidationArguments,
): boolean {
  const [relatedPropertyName] = args.constraints;
  const relatedValue = (args.object as unknown)[relatedPropertyName];
  return (
    typeof value === 'number' &&
    typeof relatedValue === 'number' &&
    value > relatedValue
  );
}

export function defaultIsGreaterThanMessage(
  validationArguments?: ValidationArguments,
): string {
  return `${validationArguments?.property} must be greater than ${validationArguments?.constraints[0]}`;
}

// eslint-disable-next-line @typescript-eslint/naming-convention
export function IsGreaterThan(
  property: string,
  validationOptions?: ValidationOptions,
) {
  return function (object: object, propertyName: string): void {
    registerDecorator({
      name: 'IsGreaterThan',
      target: object.constructor,
      propertyName,
      constraints: [property],
      options: validationOptions,
      validator: {
        validate: isGreaterThanValidate,
        defaultMessage: defaultIsGreaterThanMessage,
      },
    });
  };
}
