import {
  defaultIsGreaterThanMessage,
  IsGreaterThan,
  isGreaterThanValidate,
} from './is-greater-than.validator';

describe('tests for isGreaterThan validator', () => {
  it('should return true', () => {
    const validationResult = isGreaterThanValidate(20, {
      object: { other: 10 },
      property: 'fake',
      targetName: 'idk',
      value: 20,
      constraints: ['other'],
    });
    expect(validationResult).toBeTruthy();
  });

  it('should return false', () => {
    const validationResult = isGreaterThanValidate(20, {
      object: { other: 100 },
      property: 'fake',
      targetName: 'idk',
      value: 20,
      constraints: ['other'],
    });
    expect(validationResult).toBeFalsy();
  });

  it('should register the validator', () => {
    const validationFunction = IsGreaterThan('fake');
    validationFunction({}, 'fake');
  });

  it('should return an error string', () => {
    defaultIsGreaterThanMessage();
  });
});
