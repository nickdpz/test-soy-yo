import { Test } from '@nestjs/testing';

import { HealthController } from './health.controller';

describe('tests for HealthController', () => {
  let controller: HealthController;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [HealthController],
    }).compile();

    controller = module.get<HealthController>(HealthController);
  });

  describe('tests for findByCountry', () => {
    it('should call validate app user in the service', async () => {
      expect(controller.check()).toEqual({ status: 200 });
    });
  });
});
