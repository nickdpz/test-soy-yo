import { Test, TestingModule } from '@nestjs/testing';

import { AppController } from './app.controller';
import { AppService } from '../services/app.service';
import { ListEntitiesRequestDto } from '../dtos/app.dto';
import { AppServiceMock } from '__mocks__/services/app.service.mock';
import { DATA_RESPONSE_SERVICE } from '__mocks__/data/app.data';

describe('AppController', () => {
  let appController: AppController;
  let service: AppService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [
        {
          provide: AppService,
          useFactory: AppServiceMock,
        },
      ],
    }).compile();

    appController = module.get<AppController>(AppController);
    service = module.get<AppService>(AppService);
  });

  it('should return service response', async () => {
    jest
      .spyOn(service, 'listEntitiesByFilters')
      .mockImplementation(() => Promise.resolve(DATA_RESPONSE_SERVICE));
    await expect(
      appController.filterPayload(new ListEntitiesRequestDto()),
    ).resolves.toEqual(DATA_RESPONSE_SERVICE);
  });
});
