import {
  ApiOkResponse,
  ApiOperation,
  ApiProduces,
  ApiTags,
} from '@nestjs/swagger';
import { Controller, Get, HttpStatus } from '@nestjs/common';

import { HealthResponseDto } from '../dtos/health.dto';

@Controller('/')
@ApiTags('Health')
export class HealthController {
  @Get(['', 'health'])
  @ApiOperation({
    summary: 'Check health',
  })
  @ApiOkResponse({
    description: 'Success response',
    type: HealthResponseDto,
  })
  @ApiProduces('application/json')
  check(): HealthResponseDto {
    return {
      status: HttpStatus.OK,
    };
  }
}
