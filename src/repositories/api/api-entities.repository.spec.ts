import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import MockAdapter from 'axios-mock-adapter';
import axios from 'axios';

import { ApiEntitiesRepository } from './api-entities.repository';
import { RESULT_SERVICE } from '__mocks__/data/app.data';
const BASEURL = 'https://api.treinta.co';

describe('ApiEntitiesRepository', () => {
  let repository: ApiEntitiesRepository;
  let configService: ConfigService;

  let axiosMock: MockAdapter;
  beforeAll(() => {
    axiosMock = new MockAdapter(axios);
  });

  afterEach(() => {
    axiosMock.reset();
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ApiEntitiesRepository,
        {
          provide: ConfigService,
          useValue: { get: () => BASEURL },
        },
      ],
    }).compile();

    repository = module.get<ApiEntitiesRepository>(ApiEntitiesRepository);
    configService = module.get<ConfigService>(ConfigService);
  });

  it('test for getEntity', async () => {
    jest.spyOn(configService, 'get').mockImplementation(() => BASEURL);
    axiosMock.onGet(`${BASEURL}/1`).reply(200, RESULT_SERVICE);

    await expect(repository.getEntity(1)).resolves.toEqual(RESULT_SERVICE);
  });
});
