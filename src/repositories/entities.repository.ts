export interface GetEntityResultModel {
  code: string;
  message: string;
  type: string;
  traceId: string;
  data: {
    entityId: number;
    name: string;
    identificationNumber: string;
    attributeValidator: string | null;
    expirationDate: string;
    contactName: string;
    contactMail: string;
    ipAddress: string;
    logo: string;
    domain: string | null;
  };
}

export interface EntitiesRepository {
  getEntity(id: number): Promise<GetEntityResultModel>;
}
