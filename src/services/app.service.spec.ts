import { Test, TestingModule } from '@nestjs/testing';

import { EntitiesRepository } from 'repositories/entities.repository';
import { RESULT_SERVICE } from '__mocks__/data/app.data';
import { DATA_RESPONSE_SERVICE_ONE } from '../../test/__mocks__/data/results.data';
import { ENTITIES_REPOSITORY } from '../common/constants';
import { AppService } from './app.service';

describe('AppService', () => {
  let service: AppService;
  const repository: EntitiesRepository = {
    getEntity: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AppService,
        { provide: ENTITIES_REPOSITORY, useValue: repository },
      ],
    }).compile();

    service = module.get<AppService>(AppService);
  });

  it('listEntitiesByFilters should be return list of entities', async () => {
    jest.spyOn(repository,'getEntity').mockResolvedValue(RESULT_SERVICE);

    await expect(
      service.listEntitiesByFilters({ startId: 1, endId: 1 }),
    ).resolves.toEqual(DATA_RESPONSE_SERVICE_ONE);
  });
});
