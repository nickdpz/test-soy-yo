export default () => ({
  port: parseInt(process.env.PORT) || 3000,
  get_entity_url: process.env.GET_ENTITY_URL,
});
