import { ConfigService } from '@nestjs/config';
import { MockType } from './mocks.type';

export const ConfigServiceMock: () => MockType<ConfigService> = () => ({
  get: jest.fn(),
});
