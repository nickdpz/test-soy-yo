import { FilterEntitiesResponseModel } from '../../models/app.model';

export const DATA_RESPONSE_SERVICE: FilterEntitiesResponseModel = {
  items: [
    {
      contactEmail: 'APalacioH@tuya.com.co',
      expirationDate: '2030-10-27',
      contactName: 'Ana Maria Palacio',
      identificationNumber: '123456789',
      logo: 'logo_entidad_tuya.png',
      name: 'Tuya',
      entityId: 1,
    },
    {
      contactEmail: 'msflore@bancolombia.com.co',
      expirationDate: '2030-10-27',
      contactName: 'Mauricio Serna Florez',
      identificationNumber: '987654321',
      logo: 'logo_entidad_bancolombia.png',
      name: 'Bancolombia',
      entityId: 2,
    },
  ],
};

export const RESULT_SERVICE = {
  code: 'F132',
  message: 'Data found',
  type: 'success',
  data: {
    entityId: 1,
    name: 'Tuya',
    identificationNumber: '123456789',
    attributeValidator: null,
    expirationDate: '2030-10-27',
    contactName: 'Ana Maria Palacio',
    contactMail: 'APalacioH@tuya.com.co',
    ipAddress: '',
    logo: 'logo_entidad_tuya.png',
    domain: null,
  },
  traceId: '369917',
};
