import { AppService } from '../../services/app.service';
import { MockType } from '../mocks.type';

export const AppServiceMock: () => MockType<AppService> = () => ({
  listEntitiesByFilters: jest.fn(),
});
