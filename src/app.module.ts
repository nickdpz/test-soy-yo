import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { ENTITIES_REPOSITORY } from './common/constants';
import configuration from './config/configuration';
import { AppController } from './controllers/app.controller';
import { HealthController } from './controllers/health.controller';
import { ApiEntitiesRepository } from './repositories/api/api-entities.repository';
import { AppService } from './services/app.service';

@Module({
  imports: [
    ConfigModule.forRoot({ envFilePath: '.env', load: [configuration] }),
  ],
  controllers: [AppController, HealthController],
  providers: [
    {
      provide: ENTITIES_REPOSITORY,
      useClass: ApiEntitiesRepository,
    },
    AppService,
  ],
})
export class AppModule {}
