export interface FilterEntitiesRequestModel {
  startId: number;
  endId: number;
}

export interface FilterEntitiesItemResponseModel {
  entityId?: number;
  name: string;
  identificationNumber: string;
  expirationDate: string;
  contactName: string;
  contactEmail: string;
  logo: string;
}

export interface FilterEntitiesResponseModel {
  items: FilterEntitiesItemResponseModel[];
}
