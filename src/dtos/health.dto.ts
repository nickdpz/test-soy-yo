import { ApiProperty } from '@nestjs/swagger';

export class HealthResponseDto {
  @ApiProperty({
    type: 'object',
    properties: {
      status: { type: 'number' },
    },
  })
  status: number;
}
