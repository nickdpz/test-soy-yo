export const DATA_RESPONSE_SERVICE = {
  items: [
    {
      contactEmail: 'APalacioH@tuya.com.co',
      expirationDate: '2030-10-27',
      contactName: 'Ana Maria Palacio',
      identificationNumber: '123456789',
      logo: 'logo_entidad_tuya.png',
      name: 'Tuya',
      entityId: 1,
    },
    {
      contactEmail: 'msflore@bancolombia.com.co',
      expirationDate: '2030-10-27',
      contactName: 'Mauricio Serna Florez',
      identificationNumber: '987654321',
      logo: 'logo_entidad_bancolombia.png',
      name: 'Bancolombia',
      entityId: 2,
    },
  ],
};

export const DATA_RESPONSE_SERVICE_ONE = {
  items: [
    {
      contactEmail: 'APalacioH@tuya.com.co',
      expirationDate: '2030-10-27',
      contactName: 'Ana Maria Palacio',
      identificationNumber: '123456789',
      logo: 'logo_entidad_tuya.png',
      name: 'Tuya',
      entityId: 1,
    },
  ],
};
