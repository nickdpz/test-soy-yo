# Test Soy Yo

## Manual deploy

1. Config environment

- Create file .env

```sh
$ cat .env.example >> .env
```

2. Replace variables

3. Pull images
```sh
$ docker pull node:lts-alpine
```

4. Build app
```sh
$ docker build  --no-cache -f ./docker/Dockerfile -t soyyo-backend .
```

5. Run container
```sh
$ docker run --rm -d -p 3000:3000 --name soyyo-backend-container soyyo-backend
```

## Testing

- Run test unit

```sh
$ npm run test
```

- Run coverage test unit

```sh
$ npm run coverage
```

```
| % Stmts | % Branch | % Funcs | % Lines |
|---------|----------|---------|---------|
|   96.55 |    66.66 |     100 |   95.91 |
```

- Run test e2e

```sh
$ npm run e2e
```
