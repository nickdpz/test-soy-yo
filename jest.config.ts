/*
 * For a detailed explanation regarding each configuration property and type check, visit:
 * https://jestjs.io/docs/en/configuration.html
 */

export default {
  moduleFileExtensions: ['js', 'json', 'ts'],
  rootDir: './src',
  testRegex: '.*\\.spec\\.ts$',
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest',
  },
  collectCoverageFrom: [
    '**/*.(t|j)s',
    '!**/*.module.ts',
    '!**/*.factory.ts',
    '!**/constants.ts',
    '!**/commons/seeders/*.ts',
    '!**/commons/migrations/*.ts',
  ],
  coverageDirectory: '../coverage',
  testEnvironment: 'node',
  clearMocks: true,
  resetMocks: true,
  restoreMocks: true,
  moduleDirectories: ['node_modules', 'src'],
  moduleNameMapper: {
    '@commons/(.*)': '<rootDir>/commons/$1',
  },
  coveragePathIgnorePatterns: [
    '/node_modules/',
    '/test/',
    '^.*\\.module\\.[jt]sx?$',
    '^.*\\.dto\\.[jt]sx?$',
    '^.*\\.entity\\.[jt]sx?$',
    '^.*\\.factory\\.[jt]sx?$',
    '^.*\\.middleware\\.[jt]sx?$',
    '^constants\\.[jt]sx?$',
    '<rootDir>/config/configuration.ts',
    '<rootDir>/main.ts',
  ],
  coverageThreshold: {
    global: {
      lines: 60,
      branches: 20,
      functions: 60,
    },
  },
  setupFiles: ['dotenv/config'],
};
